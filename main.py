import sys
import sdl2.ext

RESOURCES = sdl2.ext.Resources(__file__, "resources")  #Sets resource directory

def run():

    sdl2.ext.init() #Gives access to the screen

    window = sdl2.ext.Window("Hello, World!", size=(640, 480)) #Creates a window with a title & size
    window.show() #Shows the window at launch

    factory = sdl2.ext.SpriteFactory(sdl2.ext.SOFTWARE) #Sets the sprite type to Software
    sprite = factory.from_image(RESOURCES.get_path("photo.bmp")) #Turns an image into a drawable sprite
    sprite.position = (320 - 100, 240 - 100) #Sets the sprite to the center of the screen

    spriteRenderer = factory.create_sprite_render_system(window) #Allows the sprite to be drawn
    spriteRenderer.render(sprite) #Draws the sprite onto the window

    processor = sdl2.ext.TestEventProcessor()
    processor.run(window)#Starts the event loop, keeping the window open

    sdl2.ext.quit()
    return 0

if __name__ == "__main__":
    sys.exit(run())